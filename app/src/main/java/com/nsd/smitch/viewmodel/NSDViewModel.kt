package com.nsd.smitch.viewmodel

import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nsd.smitch.model.ScanResult
import com.nsd.smitch.utils.AppConstants
import timber.log.Timber
import javax.inject.Inject

class NSDViewModel @Inject constructor() : ViewModel(), LifecycleObserver {

    var mNsdManager: NsdManager? = null
    private var serviceListObservable = MutableLiveData<List<ScanResult>>()
    private var serviceList = mutableListOf<ScanResult>()
    fun getServicesObservable(): MutableLiveData<List<ScanResult>> {
        return serviceListObservable
    }

    fun registerNsdService(serviceName: String, serviceType: String, portNumber: Int) {
        val serviceInfo = NsdServiceInfo()
        serviceInfo.serviceName = serviceName
        serviceInfo.serviceType = serviceType
        serviceInfo.port = portNumber
        mNsdManager?.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, mRegistrationListener)
    }

    fun discoverNsdService() {
        Timber.d("discoverNsdService called ")
        mNsdManager?.discoverServices(
            AppConstants.SERVICE_TYPE,
            NsdManager.PROTOCOL_DNS_SD,
            mDiscoveryListener
        )
    }

    var mRegistrationListener: NsdManager.RegistrationListener =
        object : NsdManager.RegistrationListener {
            override fun onUnregistrationFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
                val mServiceName = serviceInfo?.serviceName
                Timber.d("onUnregistrationFailed : $mServiceName + Error code : $errorCode ")
            }

            override fun onServiceUnregistered(serviceInfo: NsdServiceInfo?) {
                val mServiceName = serviceInfo?.serviceName
                Timber.d("onServiceUnregistered : $mServiceName   ")
            }

            override fun onRegistrationFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
                val mServiceName = serviceInfo?.serviceName
                Timber.d("onRegistrationFailed : $mServiceName + Error code : $errorCode ")
            }

            override fun onServiceRegistered(serviceInfo: NsdServiceInfo?) {
                val mServiceName = serviceInfo?.serviceName
                Timber.d("onServiceRegistered : $mServiceName")
            }
        }


    private var mDiscoveryListener: NsdManager.DiscoveryListener = object : NsdManager.DiscoveryListener {
        override fun onServiceFound(serviceInfo: NsdServiceInfo?) {
            val mServiceName = serviceInfo?.serviceName
            Timber.d("onServiceFound : $mServiceName")
            val scanResult = ScanResult(
                serviceInfo?.serviceName!!,
                serviceInfo.serviceType,
                if(serviceInfo.host != null)serviceInfo.host.toString() else "---",
                serviceInfo.port
            )

            serviceList.add(scanResult)
            serviceListObservable.postValue(serviceList)

            if (!serviceInfo?.serviceType.equals(AppConstants.SERVICE_TYPE))
                Timber.d("Unknown Service Type: %s", serviceInfo?.serviceType)
            else if (serviceInfo?.serviceName.equals(AppConstants.SERVICE_NAME))
                Timber.d("Same machine: %s", AppConstants.SERVICE_NAME)
            else {
                Timber.d("Diff Machine : %s", serviceInfo?.serviceName)
                mNsdManager?.resolveService(serviceInfo, mResolveListener)
            }
        }

        override fun onStopDiscoveryFailed(serviceType: String?, errorCode: Int) {
            Timber.d("onStopDiscoveryFailed : $serviceType errorCode : $errorCode")
            mNsdManager?.stopServiceDiscovery(this)
        }

        override fun onStartDiscoveryFailed(serviceType: String?, errorCode: Int) {
            Timber.d("onStartDiscoveryFailed : $serviceType errorCode : $errorCode")
            mNsdManager?.stopServiceDiscovery(this)
        }

        override fun onDiscoveryStarted(serviceType: String?) {
            Timber.d("onDiscoveryStarted : $serviceType")
        }

        override fun onDiscoveryStopped(serviceType: String?) {
            Timber.d("onDiscoveryStopped : $serviceType")
        }

        override fun onServiceLost(serviceInfo: NsdServiceInfo?) {
            val mServiceName = serviceInfo?.serviceName
            Timber.d("onServiceLost : $mServiceName")
        }


    }
    var mResolveListener: NsdManager.ResolveListener = object : NsdManager.ResolveListener {

        override fun onResolveFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
            Timber.d("Resolve Failed. %s%d ", serviceInfo, errorCode)
        }

        override fun onServiceResolved(serviceInfo: NsdServiceInfo?) {
            Timber.d("Resolve Succeeded. %s", serviceInfo)
        }
    }

    fun setNsdManager(manager: NsdManager) {
        mNsdManager = manager
    }
}


//            if (serviceInfo.getServiceName().equals(SERVICE_NAME))
//            {
//                Timber.d(TAG, "Same IP.")
//                return
//            }
//            // Obtain port and IP
//            hostPort = serviceInfo.getPort()
//            hostAddress = serviceInfo.getHost()

