package com.nsd.smitch.di.annotations.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContext
