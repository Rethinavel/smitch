
package com.nsd.smitch.di.modules


import com.nsd.smitch.base.BaseActivity
import com.nsd.smitch.base.BaseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class BaseModule {

  @ContributesAndroidInjector
  internal abstract fun contributeViewModelActivity(): BaseActivity

  @ContributesAndroidInjector
  internal abstract fun contributeViewModelFragment(): BaseFragment
}
