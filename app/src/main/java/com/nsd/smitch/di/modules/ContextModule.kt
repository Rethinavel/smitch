package com.nsd.smitch.di.modules

import android.content.Context
import com.nsd.smitch.di.annotations.ApplicationScope
import com.nsd.smitch.di.annotations.qualifier.ApplicationContext
import dagger.Module
import dagger.Provides

@Module
class ContextModule(private val context: Context) {
    @Provides
    @ApplicationScope
    @ApplicationContext
    fun provideContext(): Context {
        return context
    }

}