package com.nsd.smitch.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nsd.smitch.di.annotations.ViewModelKey
import com.nsd.smitch.di.factory.AppViewModelFactory
import com.nsd.smitch.viewmodel.NSDViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(NSDViewModel::class)
    abstract fun binsStationListViewModel(viewModel: NSDViewModel): ViewModel

}