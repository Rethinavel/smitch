package com.nsd.smitch.utils

class AppConstants {

    companion object {
        const val SERVICE_NAME = "Smitch_service"
        const val SERVICE_TYPE = "_http._tcp."
        const val PORT_NUMBER = 8080
    }

}