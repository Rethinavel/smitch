package com.nsd.smitch.model

data class ScanResult(
    val serviceName: String,
    val serviceType: String,
    val ipAddress: String,
    val portNumber: Int
)