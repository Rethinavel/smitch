package com.nsd.smitch.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nsd.smitch.R
import com.nsd.smitch.databinding.AdapterServiceListBinding
import com.nsd.smitch.model.ScanResult

class ServiceAdapter(private val stationList: MutableList<ScanResult>) :
    RecyclerView.Adapter<ServiceAdapter.StationListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationListViewHolder {
        val binding = DataBindingUtil.inflate<AdapterServiceListBinding>(
            LayoutInflater.from(parent.context), R.layout.adapter_service_list, parent, false
        )
        return StationListViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return stationList.size
    }

    override fun onBindViewHolder(holder: StationListViewHolder, position: Int) {
        holder.binding.data = stationList.get(position)
        holder.binding.executePendingBindings()
    }

    class StationListViewHolder(val binding: AdapterServiceListBinding) :
        RecyclerView.ViewHolder(binding.root)
}
