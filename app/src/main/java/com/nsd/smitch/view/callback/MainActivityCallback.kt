package com.nsd.smitch.view.callback

import android.view.View

interface MainActivityCallback {
    fun onPublish(view: View)
    fun onScan(view: View)
}