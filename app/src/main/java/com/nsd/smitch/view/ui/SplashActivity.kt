package com.nsd.smitch.view.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nsd.smitch.R
import com.nsd.smitch.utils.ConnectivityMonitor
import com.nsd.smitch.utils.extensions.toast

class SplashActivity : AppCompatActivity() {

    // For Cold Start of the system.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ConnectivityMonitor.getInstance(this).startListening {
            if (it)
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))

        }
    }
}