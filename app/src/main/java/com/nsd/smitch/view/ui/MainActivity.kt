package com.nsd.smitch.view.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.net.nsd.NsdManager
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.nsd.smitch.R
import com.nsd.smitch.base.BaseActivity
import com.nsd.smitch.databinding.ActivityMainBinding
import com.nsd.smitch.utils.AppConstants
import com.nsd.smitch.utils.extensions.initToolbar
import com.nsd.smitch.utils.extensions.toast
import com.nsd.smitch.view.adapter.ServiceAdapter
import com.nsd.smitch.view.callback.MainActivityCallback
import com.nsd.smitch.viewmodel.NSDViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainActivityCallback {

    private lateinit var serviceAdapter: ServiceAdapter
    private lateinit var mNsdManager: NsdManager
    private var serviceList = mutableListOf<com.nsd.smitch.model.ScanResult>()
    private val viewModel: NSDViewModel by injectViewModels()
    private val binding: ActivityMainBinding by binding(R.layout.activity_main)

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        initialize()
    }

    private fun initialize() {
        val recyclerLayoutManager = LinearLayoutManager(this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = recyclerLayoutManager
        }
        mNsdManager = this.getSystemService(Context.NSD_SERVICE) as NsdManager
        viewModel.setNsdManager(mNsdManager)
        binding.callback = this
        initToolbar(binding.toolbar, getString(R.string.title_smitch), false)
    }

    override fun onPublish(view: View) {
        toast(getString(R.string.message_publish_nsd_service))
        try {
            viewModel.registerNsdService(
                AppConstants.SERVICE_NAME,
                AppConstants.SERVICE_TYPE,
                AppConstants.PORT_NUMBER
            )
        } catch (e: IllegalArgumentException) {
            toast(getString(R.string.message_service_already_published))
        }
    }

    override fun onScan(view: View) {

        try {
            viewModel.discoverNsdService()
            toast(getString(R.string.message_discovery_started))
        } catch (e: IllegalArgumentException) {
            toast(getString(R.string.message_nsd_scan_already_in_process))
        }

        viewModel.getServicesObservable().observe(this, androidx.lifecycle.Observer {
            if (it != null && it.isNotEmpty()) {
                serviceList.clear()
                serviceList.addAll(it)
                serviceAdapter = ServiceAdapter(serviceList)
                recyclerView.adapter = serviceAdapter
                serviceAdapter.notifyDataSetChanged()
            } else
                toast(getString(R.string.error_message_no_service_found))
        })
    }

    override fun onDestroy() {
        mNsdManager.unregisterService(viewModel.mRegistrationListener)
        super.onDestroy()
    }
}